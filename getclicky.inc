<?php


/**
 *
 * @name _getclicky_get_profile_field
 *
 * @Param
 *
 * @return an array with all the fields from core profile module.
 *
 */
function _getclicky_get_profile_field() {

  //load all the fields
  $result = db_query('SELECT title, name, type, category, fid, weight FROM {profile_fields} ORDER BY category, weight');

  //build the array.
  $fields = array();
  while ($field = db_fetch_object($result)) {
    $fields[] = $field;
  }
  //return the result.
  return $fields;
}
